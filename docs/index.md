# Welcome to Chai 

<span style="font-size:larger;">
 **For an interactive notebook visit 
 [Quick AI Tutorial - Google Colaboratory](https://colab.research.google.com/drive/1YB6mQkcmisEBIHOjvGnWi0z9lLiaRNT5). **
 
 **There, you can build and upload your chat AI from within your browser.**
</span>

To check out the source code of this tutorial, see the repo at
[Chai Quickstart - GitHub](https://github.com/chai-nexus/chai_py_quickstart).
## Getting Started

You can create a simple chat AI within 5 minutes, starting with these steps:

1. Head over to the [Chai Developer Platform](https://chai.ml/dev).
2. Sign in with Google
3. Scroll to the bottom to see your "Developer Unique ID" (referred to as `developer_uid` in this tutorial).

### Installation 

Using pip to install chai_py.

```
# Installing the chai_py package. 
!pip install --extra-index-url https://test.pypi.org/simple/ --upgrade chaipy
```
This `pip install` command also installs the following requirements:

* `requests` to help verify the chatbot when packaging it.
* `colorama` for colorful logs!
* `typing-extensions` for type hints used by the library introduced in later versions of Python (backported to Python 3.7 in this library).


## Creating Your First Chat AI 

#### A simple bot class

Create a file called bot.py. This should contain only the class
for your bot, and the imports.

```python 
from chai_py import ChaiBot, Update

class Bot(ChaiBot):
    def setup(self):
        self.logger.info("Setting up...")

    async def on_message(self, update: Update) -> str:
        return "Hi, I'm ExampleBot."
```

* `Update` - contains a `LatestMessage`, which contains
the text the user sends to the bot (`update.latest_message.text`) and a timestamp (`update.latest_message.timestamp`).

* The `ChaiBot` class does some convenient setup for us, and provides us with `self.logger` which we'll use to say "Setting up...".

* We override the `on_message()` method and return a string. Note the signature of this method (i.e. the arguments it takes, and its return type). This cannot be changed: the bot should always return a string.


### Chatting with the Bot

TRoom allows you to locally chat and test out your bot before uploading it.

```python
from chai_py import TRoom

t_room = TRoom([Bot()])
t_room.start()
```

### Packaging

Let's package the bot into a zip archive to prepare for upload.

```python
from chai_py import package, Metadata

DEVELOPER_UID = "get_this_from_chai_developer_platform"

package(
    Metadata(
        name="Example Bot",
        image_url="https://picsum.photos/seed/example_bot/256/256",
        color="f1a2b3",
        developer_uid=DEVELOPER_UID,
        description="I am an example bot.",
        input_class=Bot,
    )
)
```
### Deploying and Uploading

Now, we'll upload our finished bot to the chai app! 
From there, you and other users can chat to it.

```python
from chai_py import upload_and_deploy, wait_for_deployment

DEVELOPER_UID = "get_this_from_chai_developer_platform"
DEVELOPER_KEY = "and_get_this_from_chai_developer_platform"

bot_uid = upload_and_deploy(
    "package.zip",
    uid=DEVELOPER_UID,
    key=DEVELOPER_UID
    )
wait_for_deployment(bot_uid)

bot_url = advertise_deployed_bot(bot_uid)
print(bot_url)
```

The bot_url will take you straight to your bot in the chai app!

**To see the full code, check out [the GitHub repo](https://github.com/chai-nexus/chai_py_quickstart).**

## What's Next?

Check out some of our other tutorials, such as [dialogpt], or [echo-bot]
or create some of your own:

**Beginner**

* Chai has a part to play in fulfilling the fundamental purpose of the internet: 
You can play your part with an ASCII cat pictures bot.

```
("`-''-/").___..--''"`-._ 
 `6_ 6  )   `-.  (     ).`-.__.`) 
 (_Y_.)'  ._   )  `._ `. ``-..-' 
   _..`--'_..-_/  /--'_.'
  ((((.-''  ((((.'  (((.-' 
```

**Intermediate**

* Don't let your memes be dreams! Make a bot that'll pull a relevant meme for every situation!

**Advanced**

* Finetune a Deep-NN bot on quotes from your favourite fictional character!
