# EchoBot

This tutorial demonstrates the `__start` initial message. 
(Viewable only when speaking to the bot in-app).

### Creating bot.py
```python
from chai_py import ChaiBot, Update

class EchoBot(ChaiBot):
    def setup(self):
        self.logger.info("Hello, world!")

    async def on_message(self, update: Update) -> str:
        return f"Echo: {update.latest_message.text}"
```
### TRoom Output

```
>>> from chai_py import TRoom
>>> from bot import EchoBot
>>> t_room = TRoom([EchoBot()])
>>> t_room.start()

2021-03-07 17:07:17 | INFO     | EchoBot | Hello, world!
Starting TRoom. Press ctrl-c to escape.
<<< __first
>>> Echo: __first
Enter your message: Hello there!
<<< Hello there!
>>> Echo: Hello there!
Enter your message: Stop copying me!
<<< Stop copying me!
>>> Echo: Stop copying me!
Enter your message: Oh gosh.
<<< Oh gosh.
>>> Echo: Oh gosh.

```
## Packaging

```python

from chai_py import package, Metadata

DEVELOPER_UID = "your_uid"
package(
    Metadata(
        name="EchoBot",
        image_url="http://picsum.photos/seed/echo_bot/256/256",
        color="f1a2b3",
        developer_uid=DEVELOPER_UID,
        description="Do you hear something?",
        input_class=EchoBot,
    )
)
```

## Deploying and Uploading

Now, we'll upload EchoBot to the chai app! From there, you and other users can chat to it.

```python
from chai_py import upload_and_deploy, wait_for_deployment

DEVELOPER_UID = "your_uid"
DEVELOPER_KEY = "your_key"

bot_uid = upload_and_deploy(
    "package.zip",
    uid=DEVELOPER_UID,
    key=DEVELOPER_UID
    )
wait_for_deployment(bot_uid)

bot_url = advertise_deployed_bot(bot_uid)
print(bot_url)
```
