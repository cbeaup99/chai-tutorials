# DialoGPT

In this tutorial you'll learn how to upload a state-of-the-art machine learning bot that
learns from the user! All within a few lines of code.

First, we need to install transformers.

``pip install transformers``

## **bot.py**
```python
from chai_py import ChaiBot, Update

from transformers import AutoModelForCausalLM, AutoTokenizer
import torch

class DialoGptBot(ChaiBot):
    MODEL_SIZE = "medium"
    # "small": 1.5M Tokenizer, 336M Model
    # "medium": 1.5M Tokenizer, 823M Model
    # "large": 1.5M Tokenizer, 1.7G Model
    
    def setup(self):
        self.logger.info("Starting setup...")
        self.tokenizer = AutoTokenizer.from_pretrained(f"microsoft/DialoGPT-{self.MODEL_SIZE}")
        self.model = AutoModelForCausalLM.from_pretrained(f"microsoft/DialoGPT-{self.MODEL_SIZE}")

        self.logger.info("Hello, world!")

    async def on_message(self, update: Update) -> str:
        if update.latest_message.text == "__first":
            return f"Hi, I'm a {self.MODEL_SIZE} DialoGPT chatbot!"

        messages = await self.get_messages(update.conversation_id)
        self.logger.info(f"Received {len(messages)} messages.")
        chat_history_ids = []
        for message in messages:
            if message.content != "__first":
                # Encode the input, add the eos_token and return a tensor in PyTorch
                self.logger.info(f"Encoding: {message.content}")
                ids = self.tokenizer.encode(
                    message.content + self.tokenizer.eos_token, 
                    return_tensors='pt',
                )
                chat_history_ids.append(ids)
        
        # Concatenate ids from previous messages to be used as model input
        bot_input_ids = torch.cat(chat_history_ids, dim=-1)

        # Run model
        chat_history_ids = self.model.generate(
            bot_input_ids, 
            max_length=1000,
            pad_token_id=self.tokenizer.eos_token_id,
        )

        # Use last output tokens from bot
        return self.tokenizer.decode(chat_history_ids[:, bot_input_ids.shape[-1]:][0], skip_special_tokens=True)
```

## Interacting with the DialoGPT AI

Use the following command to test it out and chat to it.

You can do this in your browser
with [our notebook here](https://colab.research.google.com/drive/1YB6mQkcmisEBIHOjvGnWi0z9lLiaRNT5#scrollTo=I_B6HON6Pkpz).
```python 
from chai_py import TRoom

t_room = TRoom([DialoGptBot()])
t_room.start()
```


## Packaging, uploading and deploying

```python
from chai_py import Metadata, package, upload_and_deploy, wait_for_deployment
from chai_py.deployment import advertise_deployed_bot
from bot import DialoGptBot

DEVELOPER_UID = "developer_unique_id_goes_here"
DEVELOPER_KEY = "key_goes_here"

bot_image_url = "https://picsum.photos/seed/bot/300/300"

package(
    Metadata(
        name="DialoGPT",
        image_url=bot_image_url,
        color="f1a2b3",
        developer_uid=DEVELOPER_UID,
        description="Made with transformers",
        input_class=DialoGptBot,
        memory=3000,
    ),
    requirements=["transformers[torch]"],
)


bot_uid = upload_and_deploy(
    "package.zip", uid=DEVELOPER_UID,
    key=DEVELOPER_KEY
)
wait_for_deployment(bot_uid)

bot_url = advertise_deployed_bot(bot_uid)
```

Notice how `package` took a different argument this time? You need
to supply it with `requirements` and a larger `memory` allocation
for this bot.
