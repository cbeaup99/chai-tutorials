# Docs

### Packaging Constraints


* Root bot folder cannot have a file named "main.py" or any files starting
with a leading underscore.
* File names with a leading underscore (e.g. _temp.py) will be ignored in 
the packaging process.

### Deployment Constraints

- Maximum size (HARD LIMIT)
    - 100 MB for compressed sources
    - 500 MB for (uncompressed) sources + modules
- Maximum memory
      - 256 MB (default), up to 4GB
      - HARD LIMIT: 4GB
- Maximum Duration
      - 10s (default), up to 3 minutes
      - HARD LIMIT: 60s (default), up to 9 minutes
